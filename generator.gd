extends Node2D

const SIZE = Vector2( 50, 37)

var map = []
var tilemap

func _ready():
	# get TileMap node
	tilemap = get_node("TileMap")

# Random button pressed
func _on_Random_pressed():
	map.resize(SIZE.x * SIZE.y)
	for y in range(SIZE.y):
		for x in range(SIZE.x):
			var i = y * SIZE.x + x # index of current tile
			var fill_percent = get_node("Buttons/Panel/Fill").get_val() # how much walls we want
			
			# fill map with random tiles
			if randi() % 101 < fill_percent or x == 0 or x == SIZE.x - 1 or y == 0 or y == SIZE.y - 1:
				map[i] = 1 # wall
			else:
				map[i] = 0 # empty
	# draw the map
	update_map()
	get_node("Buttons/Smooth").set_disabled(false) # when we have a map user can smooth it

# set tiles in Tilemap to match map array
func update_map():
	for y in range(SIZE.y):
		for x in range(SIZE.x):
			var i = y * SIZE.x + x
			tilemap.set_cell(x, y, map[i])

func _on_Smooth_pressed():
	# new map to apply changes
	var new_map = []
	new_map.resize(SIZE.x * SIZE.y)
	
	for e in range(map.size()): # copy old array
		new_map[e] = map[e]
	
	# we need to skip borders of screen
	for y in range(1,SIZE.y -1):
		for x in range(1,SIZE.x - 1):
			var i = y * SIZE.x + x
			if map[i] == 1: # if it was a wall
				if touching_walls(Vector2(x,y)) >= 4: # and 4 or more of its eight neighbors were walls
					new_map[i] = 1 # it becomes a wall
				else:
					new_map[i] = 0
			elif map[i] == 0: # if it was empty
				if touching_walls(Vector2(x,y)) >= 5: # we need 5 or neighbors
					new_map[i] = 1
				else:
					new_map[i] = 0
	map = new_map # apply new array
	update_map()
	
# return count of touching walls 
func touching_walls(point):
	var result = 0
	for y in [-1,0,1]:
		for x in [-1,0,1]:
			if x == 0 and y == 0: # we don't want to count tested point
				continue
			var i = (y + point.y) * SIZE.x + (x + point.x)
			if map[i] == 1:
				result += 1
	return result
	